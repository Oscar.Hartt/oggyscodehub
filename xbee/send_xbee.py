from digi.xbee.devices import XBeeDevice
from discover_xbee import Discover

class send_message():
    
    def __init__(self, xbee):
        self.xbee = xbee

    def send_setup(self, send_data):
        self.data_to_send = send_data 
        self.sendIt()

    def sendIt(self, remote_xbee, data_to_send):
        self.xbee.send_data(remote_xbee, data_to_send)
