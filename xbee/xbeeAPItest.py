from digi.xbee.devices import XBeeDevice
import RPi.GPIO as GPIO
import time
from ADCDevice import *

ledPin = 11
adc = ADCDevice() # Define an ADCDevice class object
xbee = XBeeDevice("/dev/ttyS0", 9600)
xbee.open()

def setup():
    global adc
    if(adc.detectI2C(0x48)): # Detect the pcf8591.
        adc = PCF8591()
    elif(adc.detectI2C(0x4b)): # Detect the ads7830
        adc = ADS7830()
    else:
        print("No correct I2C address found, \n"
        "Please use command 'i2cdetect -y 1' to check the I2C address! \n"
        "Program Exit. \n");
        exit(-1)

xnet = xbee.get_network()

setup()

xnet.start_discovery_process()
while xnet.is_discovery_running():
    time.sleep(0.5)


nodes = xnet.get_devices()
remote_device = xnet.discover_device(nodes[0]._node_id)
print(xnet.discover_device(nodes[0]._node_id))

msg = xbee.send_data(remote_device, "Ready to recieve commands")
try:
    while True:
        recv_msg = xbee.read_data_from(remote_device)
        if recv_msg != None:
            recv_msg = recv_msg.data.decode()
            if recv_msg == "LED on":
                GPIO.output(ledPin, GPIO.HIGH)
                xbee.send_data(remote_device, "LED on")
                print("LED ON")
            elif recv_msg == "LED off":
                GPIO.output(ledPin, GPIO.LOW)
                xbee.send_data(remote_device, "LED off")
                print("LED OFF")

except KeyboardInterrupt:   # Press ctrl-c to end the program.
    GPIO.cleanup()
    xbee.close()